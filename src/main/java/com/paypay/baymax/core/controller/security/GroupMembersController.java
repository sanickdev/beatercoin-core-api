package com.paypay.baymax.core.controller.security;

import com.nxn.sau.security.util.JWTEncripter;
import com.paypay.baymax.commons.DTO.pagination.ResultsUtil;
import com.paypay.baymax.commons.DTO.security.Group_membersDTO;
import com.paypay.baymax.commons.DTO.util.MetaDTO;
import com.paypay.baymax.commons.DTO.util.ResponseDTO;
import com.paypay.baymax.commons.meta.EndPoints;
import com.paypay.baymax.commons.meta.ErrorCodes;
import com.paypay.baymax.commons.util.DefinicionesComunes;
import com.paypay.baymax.commons.util.GsonBuild;
import com.paypay.baymax.commons.util.Modulos;
import com.paypay.baymax.core.service.combo.IComboService;
import com.paypay.baymax.core.service.security.IGroupMembersService;
import com.paypay.baymax.core.service.security.IGroupsService;
import com.paypay.baymax.core.service.security.IUsersService;
import com.paypay.baymax.domain.security.Group_members;
import com.paypay.baymax.domain.security.Groups;
import com.paypay.baymax.domain.security.Users;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Control de Cambios
 * <p>
 * ID | USUARIO | FECHA | DESCRIPCIÓN
 */

@RequestMapping(EndPoints.GROUP_MEMBERS)
@RestController
@Scope("request")
public class GroupMembersController {

	private final String modulo = Modulos.ENTIDADES.get(Group_members.class.getSimpleName());
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	private JWTEncripter jwt = new JWTEncripter(new GsonBuild().getGson());
	private final IGroupMembersService groupMembersService;
	private final IComboService comboService;
	private final IGroupsService groupService;
	private final IUsersService userService;

	@Autowired
	public GroupMembersController(IGroupMembersService groupMembersService, IComboService comboService,
			IGroupsService groupService, IUsersService userService) {
		this.groupMembersService = groupMembersService;
		this.comboService = comboService;
		this.groupService = groupService;
		this.userService = userService;
	}

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public String get(@PathVariable("id") Integer id, HttpServletRequest request) {

		log.debug("Obteniendo " + this.modulo + " por ID...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			mapa.put("groupMember", groupMembersService.convertToDTO(this.groupMembersService.getGroupMembers(id)));
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_GET + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@GetMapping(value = "/{username:.+}/groups")
	@ResponseStatus(HttpStatus.OK)
	public String getPermisosPerfil(@PathVariable("username") String username, HttpServletRequest request) {
		log.debug("Obteniendo " + this.modulo + " por ID de usuario...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			HashMap<String, String> grupos = new HashMap<>();

			List<Groups> groupsList = groupService.getAll();

			for (Groups c : groupsList) {
				grupos.put(String.valueOf(c.getId()), c.getGroup_name());
			}

			List<?> list = comboService.getGruposByUsername(username);

			List<String> groupsInUser = new ArrayList<>();
			List<Map<String, Object>> list_ = new ArrayList<Map<String, Object>>();

			if (list != null) {
				for (Object o : list) {
					@SuppressWarnings("unchecked")
					Map<String, Object> map = (Map<String, Object>) o;
					groupsInUser.add(map.get("group_id").toString());
					map.put("check", true);
					map.put("group_id", map.get("group_id").toString());
					list_.add(map);
				}
			}
			// gm.username, g.id, g.group_name
			for (Entry<String, String> grupo : grupos.entrySet()) {
				if (!groupsInUser.contains(grupo.getKey())) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("group_name", grupo.getValue());
					map.put("group_id", grupo.getKey());
					map.put("username", username);
					map.put("check", false);
					list_.add(map);
				}

			}

			mapa.put("perfilesUsuario", list_);
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_GET + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
			mapa.put("dtr", ResultsUtil.getDtResultsForException());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@GetMapping(value = "/getList")
	@ResponseStatus(HttpStatus.OK)
	public String getList(HttpServletRequest request) {
		log.debug("Obteniendo listado de " + this.modulo);

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			mapa.put("lista", groupMembersService.getGroupMembersList());
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_LIST + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@PostMapping(value = "/save")
	@ResponseStatus(HttpStatus.OK)
	public String save(@RequestBody String json, HttpServletRequest request) {

		log.debug("Registrando " + this.modulo + " en el sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			Group_membersDTO dto = jwt.fromJWT(json, Group_membersDTO.class);
			Group_members BD = groupMembersService.convertToEntity(dto);

			BD.setRecordDate(DateTime.now().toDate());

//        	if(dto.getUsername().compareTo(dto.getRecordUsername()) == 0 )
//        	{
//        		code = "VL";
//        		message = "No est\\u00E1 permitido modificar su propio perfil";
//        	}
//        	else
//        	{
			groupMembersService.add(BD);

			List<Group_members> listGM = groupMembersService.getGMByUsername(dto.getUsername());

			if (CollectionUtils.isNotEmpty(listGM)) {
				Users user = userService.get(dto.getUsername());
				try {
					user.setGroups(listGM.stream().map(g -> g.getGroup_id().getGroup_name())
							.collect(Collectors.joining(", ")));
				} catch (Exception e) {
					user.setGroups("");
				}

				user.setUpdateDate(DateTime.now().toDate());
				user.setUpdateUsername(dto.getRecordUsername());
				userService.update(user);
			}

			mapa.put("groupMember", groupMembersService.convertToDTO(BD));
//        	}
		} catch (ConstraintViolationException e) {
			code = "UK";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (DataIntegrityViolationException e) {
			code = "DT";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@PostMapping(value = "/update")
	@ResponseStatus(HttpStatus.OK)
	public String update(@RequestBody String json, HttpServletRequest request) {

		log.debug("Actualizando " + this.modulo + " del sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			Group_membersDTO dto = jwt.fromJWT(json, Group_membersDTO.class);

			Group_members BD = groupMembersService.get(dto.getId());
			Group_members BDNew = groupMembersService.convertToEntity(dto);

			BDNew.setRecordUsername(BD.getRecordUsername());
			BDNew.setRecordDate(BD.getRecordDate());
			BDNew.setUpdateDate(DateTime.now().toDate());

			if (BD.getUsername().compareTo(dto.getUpdateUsername()) == 0) {
				code = "VL";
				message = "No est\\u00E1 permitido modificar su propio perfil";
			} else {
				groupMembersService.update(BDNew);
				mapa.put("grupo", BDNew);
			}
		} catch (ConstraintViolationException e) {
			code = "UK";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (DataIntegrityViolationException e) {
			code = "DT";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));

	}

	@GetMapping(value = "/delete/{group_id}/{username:.+}/{updateUsername:.+}")
	@ResponseStatus(HttpStatus.OK)
	public String delete(@PathVariable("group_id") String group_id, @PathVariable("username") String username,
			@PathVariable("updateUsername") String updateUsername, HttpServletRequest request) {

		log.debug("Dando de baja " + this.modulo + " del sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			this.groupMembersService.deleteGM(Long.parseLong(group_id), username);

			List<Group_members> listGM = groupMembersService.getGMByUsername(username);

			if (CollectionUtils.isNotEmpty(listGM)) {
				Users user = userService.get(username);
				try {
					user.setGroups(listGM.stream().map(g -> g.getGroup_id().getGroup_name())
							.collect(Collectors.joining(", ")));
				} catch (Exception e) {
					user.setGroups("");
				}

				user.setUpdateDate(DateTime.now().toDate());
				user.setUpdateUsername(updateUsername);
				userService.update(user);
			}

		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_DELETE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}
}
