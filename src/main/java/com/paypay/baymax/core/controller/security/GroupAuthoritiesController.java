package com.paypay.baymax.core.controller.security;

import com.nxn.sau.security.util.JWTEncripter;
import com.paypay.baymax.commons.DTO.pagination.ResultsUtil;
import com.paypay.baymax.commons.DTO.security.Group_authoritiesDTO;
import com.paypay.baymax.commons.DTO.util.MetaDTO;
import com.paypay.baymax.commons.DTO.util.ResponseDTO;
import com.paypay.baymax.commons.meta.EndPoints;
import com.paypay.baymax.commons.meta.ErrorCodes;
import com.paypay.baymax.commons.security.CustomGrantedAuthorities;
import com.paypay.baymax.commons.util.DefinicionesComunes;
import com.paypay.baymax.commons.util.GsonBuild;
import com.paypay.baymax.commons.util.Modulos;
import com.paypay.baymax.core.service.combo.IComboService;
import com.paypay.baymax.core.service.security.IGroupAuthoritiesService;
import com.paypay.baymax.core.service.security.IGroupsService;
import com.paypay.baymax.domain.security.Group_authorities;
import com.paypay.baymax.domain.security.Groups;

import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@RequestMapping(EndPoints.GROUP_AUTHORITIES)
@RestController
@Scope("request")
public class GroupAuthoritiesController {

	private final String modulo = Modulos.ENTIDADES.get(Group_authorities.class.getSimpleName());
	public final Logger log = LoggerFactory.getLogger(this.getClass());
	private JWTEncripter jwt = new JWTEncripter(new GsonBuild().getGson());
	private final IGroupAuthoritiesService groupAuthoritiesService;
	private final IGroupsService groupService;
	private final IComboService comboService;

	@Autowired
	public GroupAuthoritiesController(IGroupAuthoritiesService groupAuthoritiesService, IGroupsService groupService,
			IComboService comboService) {
		this.groupAuthoritiesService = groupAuthoritiesService;
		this.groupService = groupService;
		this.comboService = comboService;
	}
	
	@GetMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public String get(@PathVariable("id") Integer id, HttpServletRequest request) {
		log.debug("Obteniendo " + this.modulo + " por ID...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			mapa.put("groupAuthorities", groupAuthoritiesService.getGroupAuthorities(id));
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_GET + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
			mapa.put("dtr", ResultsUtil.getDtResultsForException());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@GetMapping(value = "/{id}/authorities")
	@ResponseStatus(HttpStatus.OK)
	public String getGroupAuthorities(@PathVariable("id") Integer id, HttpServletRequest request) {
		log.debug("Obteniendo " + this.modulo + " por ID de perfil...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			HashMap<String, String> authorities = new HashMap<>();

			for (CustomGrantedAuthorities c : CustomGrantedAuthorities.values()) {
				authorities.put(c.getAuthority(), c.getDescripcion());
			}

			List<?> list = comboService.getPermisosByGrupo(id);

			List<String> authoritiesInGroup = new ArrayList<>();
			List<Map<String, Object>> list_ = new ArrayList<Map<String, Object>>();

			for (Object o : list) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) o;
				map.remove("id");
				map.putAll(map);
				if (map.containsKey("authority")) {
					authoritiesInGroup.add(map.get("authority").toString());
					map.put("check", true);
					map.put("authorityName", map.get("authority").toString());
					map.put("authorityValue", authorities.get(map.get("authority").toString()));
					list_.add(map);
				}
			}

			for (Entry<String, String> authority : authorities.entrySet()) {
				if (!authoritiesInGroup.contains(authority.getKey())) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("group_id", id);
					map.put("authorityName", authority);
					map.put("authorityValue", authority.getValue());
					map.put("check", false);
					list_.add(map);
				} else {

				}
			}

			mapa.put("groupAuthorities", list_);
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_GET + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
			mapa.put("dtr", ResultsUtil.getDtResultsForException());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public String save(@RequestBody String json, HttpServletRequest request) {

		log.debug("Registrando " + this.modulo + " en el sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			Group_authoritiesDTO dto = jwt.fromJWT(json, Group_authoritiesDTO.class);
			Group_authorities BD = groupAuthoritiesService.convertToEntity(dto);

			BD.setRecordDate(DateTime.now().toDate());

			groupAuthoritiesService.add(BD);

			List<Group_authorities> gaList = groupAuthoritiesService.getGAListByGroup(BD.getGroup_id().getId());

			if (CollectionUtils.isNotEmpty(gaList)) {
				HashMap<String, String> authorities = new HashMap<>();

				for (CustomGrantedAuthorities c : CustomGrantedAuthorities.values()) {
					authorities.put(c.getAuthority(), c.getDescripcion());
				}

				Groups group = groupService.get(BD.getGroup_id().getId());

				try {
					group.setAssignedAuthorities(gaList.stream().map(ga -> authorities.get(ga.getAuthority()))
							.collect(Collectors.joining(", ")));
				} catch (Exception e) {
					group.setAssignedAuthorities("");
				}

				group.setUpdateUsername(dto.getRecordUsername());
				group.setUpdateDate(DateTime.now().toDate());

				groupService.update(group);

			}

			mapa.put("groupAuthorities", BD);
		} catch (ConstraintViolationException e) {
			code = "UK";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (DataIntegrityViolationException e) {
			code = "DT";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_SAVE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public String update(@RequestBody String json, HttpServletRequest request) {

		log.debug("Actualizando " + this.modulo + " del sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			Group_authoritiesDTO dto = jwt.fromJWT(json, Group_authoritiesDTO.class);

			Group_authorities BD = groupAuthoritiesService.get(dto.getId());
			Group_authorities BDNew = groupAuthoritiesService.convertToEntity(dto);

			BDNew.setRecordUsername(BD.getRecordUsername());
			BDNew.setRecordDate(BD.getRecordDate());
			BDNew.setUpdateDate(DateTime.now().toDate());

			groupAuthoritiesService.update(BDNew);

			mapa.put("groupAuthorities", BDNew);
		} catch (ConstraintViolationException e) {
			code = "UK";
			message = ErrorCodes.ERROR_UPDATE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (DataIntegrityViolationException e) {
			code = "DT";
			message = ErrorCodes.ERROR_UPDATE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_UPDATE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));

	}

	@RequestMapping(value = "/delete/{group_id}/{authority}/{updateUsername}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public String delete(@PathVariable("group_id") String group_id, @PathVariable("authority") String authority,
			@PathVariable("updateUsername") String updateUsername, HttpServletRequest request) {

		log.debug("Dando de baja " + this.modulo + " del sistema...");

		HashMap<String, Object> mapa = new HashMap<String, Object>();
		String message = "Transacción completa";
		String code = DefinicionesComunes.CODIGO_OK;
		String type = null;
		String detail = null;

		try {
			if (authority.compareTo(CustomGrantedAuthorities.ROLE_ACCESO.getAuthority()) == 0) {
				message = "No está permitido eliminar el privilegio ["
						+ CustomGrantedAuthorities.ROLE_ACCESO.getDescripcion() + "] de ningún perfil";
				code = "NO_REMOVE_ACCESO_PAYPAY";
				type = "Validation";
				detail = message;
			} else {
				this.groupAuthoritiesService.deleteGroup_Authority(Integer.parseInt(group_id), authority);

				List<Group_authorities> gaList = groupAuthoritiesService.getGAListByGroup(Long.parseLong(group_id));

				if (CollectionUtils.isNotEmpty(gaList)) {
					HashMap<String, String> authorities = new HashMap<>();

					for (CustomGrantedAuthorities c : CustomGrantedAuthorities.values()) {
						authorities.put(c.getAuthority(), c.getDescripcion());
					}

					Groups group = groupService.get(Long.parseLong(group_id));

					try {
						group.setAssignedAuthorities(gaList.stream().map(ga -> authorities.get(ga.getAuthority()))
								.collect(Collectors.joining(", ")));
					} catch (Exception e) {
						group.setAssignedAuthorities("");
					}

					group.setUpdateUsername(updateUsername);
					group.setUpdateDate(DateTime.now().toDate());
					groupService.update(group);
				}
			}

		} catch (Exception e) {
			code = "BD";
			message = ErrorCodes.ERROR_DELETE + " de " + this.modulo;
			type = e.getClass().getSimpleName();
			detail = e.getMessage();
			log.error(message + ": " + e.getMessage());
		}

		return jwt.toJWT(new ResponseDTO(new MetaDTO(request.hashCode(), code, type, message, detail), mapa));
	}

}
