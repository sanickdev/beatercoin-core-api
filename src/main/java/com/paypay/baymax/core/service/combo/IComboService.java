package com.paypay.baymax.core.service.combo;

import java.util.List;
import java.util.concurrent.Future;
import org.springframework.scheduling.annotation.Async;

import com.paypay.baymax.commons.type.ComboListType;

public interface IComboService {
	
	ComboListType getLabels();
	ComboListType getSections();
	List<?> getGruposByUsername(String username);
	List<?> getPermisosByGrupo(Integer id);
	
	@Async
	Future<Boolean> refreshCacheOfCurrentEventEntity(String entity);
	
	

	

}
