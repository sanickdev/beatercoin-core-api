package com.paypay.baymax.core.service.combo;

import java.util.List;
import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.paypay.baymax.commons.type.ComboListType;
import com.paypay.baymax.commons.type.ComboType;
import com.paypay.baymax.commons.util.Modulos;
import com.paypay.baymax.core.converter.generales.ComboConverter;
import com.paypay.baymax.core.dao.combo.IComboDAO;

@Service
@Transactional(readOnly = true)
public class ComboService implements IComboService {

	private final IComboDAO dao;

	@Autowired
	public ComboService(IComboDAO dao) {
		this.dao = dao;
	}

	@Override
	public ComboListType getLabels() {
		ComboListType comboListType = new ComboListType();
		List<?> lstTCombo = dao.getLabels();
		List<ComboType> newsType = ComboConverter.convertListEntityToType(lstTCombo);
		comboListType.setCombo(newsType);
		return comboListType;
	}
	
	@Override
	public ComboListType getSections() {
		ComboListType comboListType = new ComboListType();
		List<?> lstTCombo = dao.getSections();
		List<ComboType> newsType = ComboConverter.convertListEntityToType(lstTCombo);
		comboListType.setCombo(newsType);
		return comboListType;
	}
	
	@Override
	public List<?> getGruposByUsername(String username) {
		return dao.getGruposByUsername(username);
	}

	@Override
	public List<?> getPermisosByGrupo(Integer id) {
		return dao.getPermisosByGrupo(id);
	}

	@Override
	public Future<Boolean> refreshCacheOfCurrentEventEntity(String entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
