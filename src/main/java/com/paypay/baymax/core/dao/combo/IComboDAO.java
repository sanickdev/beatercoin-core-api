package com.paypay.baymax.core.dao.combo;

import java.util.List;

public interface IComboDAO {
	
	List<?> getLabels();
	List<?> getSections();
	List<?> getGruposByUsername(String username);
	List<?> getPermisosByGrupo(Integer id);
	
	void refreshCacheLabels();
	void refreshCacheSections();
	
	
}
