package com.paypay.baymax.core.dao.combo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

import com.paypay.baymax.commons.constants.ConstantesCombos;
import com.paypay.baymax.domain.security.Group_authorities;
import com.paypay.baymax.domain.security.Group_members;
import com.paypay.baymax.domain.security.Groups;

@Repository
public class ComboDAO implements IComboDAO {

	private final SessionFactory sessionFactory;

	@Autowired
	public ComboDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public List<?> getGruposByUsername(String username) {
		List<?> list = sessionFactory.getCurrentSession()
				.createSQLQuery("select gm.username, gm.group_id, g.group_name from "
						+ Group_members.class.getSimpleName() + " gm " + "inner join " + Groups.class.getSimpleName()
						+ " g on gm.group_id = g.id " + "where username = :username")
				.setParameter("username", username).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		return list != null ? list : new ArrayList<>();
	}

	@Override
	public List<?> getPermisosByGrupo(Integer id) {
		List<?> list = sessionFactory.getCurrentSession()
				.createSQLQuery("select id, group_id, authority  from " + Group_authorities.class.getSimpleName()
						+ " where group_id =:group_id order by authority ")
				.setParameter("group_id", id).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		return list != null ? list : new ArrayList<>();
	}

	@Override
	@CacheEvict(value = ConstantesCombos.LABEL, allEntries = true)
	public void refreshCacheLabels() {
	}

	@Override
	@CacheEvict(value = ConstantesCombos.SECTION, allEntries = true)
	public void refreshCacheSections() {
	}

	@Override
	public List<?> getLabels() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getSections() {
		// TODO Auto-generated method stub
		return null;
	}

}
